package com.conygre.training.tradesimulator.sim;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.http.HttpClient;
import java.util.ArrayList;
import java.util.List;

import com.conygre.training.tradesimulator.dao.TradeMongoDao;
import com.conygre.training.tradesimulator.model.Trade;
import com.conygre.training.tradesimulator.model.Trade.Status;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import org.apache.tomcat.util.json.JSONParser;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class TradeSim {
    private static final Logger LOG = LoggerFactory.getLogger(TradeSim.class);

    public String getPortfolioId() throws Exception {
        String url = "http://localhost:8080/portfolio/1";
        URL obj = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
        DataOutputStream dataOutputStream = null;
        connection.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
       

        JSONObject jsonObj = new JSONObject(response.toString());

        return jsonObj.getString("objectId");
       
    }
    public boolean doRequest(Trade trade) throws Exception {

        String portfolioId = getPortfolioId();
        String url = "http://localhost:8080/portfolio/" + portfolioId;

        System.out.println(url);

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(trade);

        
        URL obj = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
        DataOutputStream dataOutputStream = null;
        connection.setRequestMethod("PUT");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        connection.setDoOutput(true);
        
        OutputStream os = connection.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
        osw.write(json);
        osw.flush();
        osw.close();

        int responseCode = connection.getResponseCode();
        
        
        return 200 == responseCode;
    }

    @Autowired
    private TradeMongoDao tradeDao;

    @Transactional
    public List<Trade> findTradesForProcessing() {
        List<Trade> foundTrades = tradeDao.findByStatus(Status.CREATED);

        for (Trade thisTrade : foundTrades) {
            thisTrade.setStatus(Status.PROCESSING);
            tradeDao.save(thisTrade);
        }

        return foundTrades;
    }

    @Transactional
    public List<Trade> findTradesForFilling() {
        List<Trade> foundTrades = tradeDao.findByStatus(Status.PROCESSING);

        for (Trade thisTrade : foundTrades) {
            if ((int) (Math.random() * 10) > 8) {
                thisTrade.setStatus(Status.REJECTED);
            } else {
                thisTrade.setStatus(Status.FILLED);  
            }
            tradeDao.save(thisTrade);
            if(thisTrade.getStatus().equals(Status.FILLED)){
                try {
                    System.out.println(doRequest(thisTrade));
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
           
        }

        return foundTrades;
    }

    @Scheduled(fixedRateString = "${scheduleRateMs:10000}")
    public void runSim() {
        LOG.debug("Main loop running!");

        int tradesForFilling = findTradesForFilling().size();
        LOG.debug("Found " + tradesForFilling + " trades to be filled/rejected");

        int tradesForProcessing = findTradesForProcessing().size();
        LOG.debug("Found " + tradesForProcessing + " trades to be processed");

    }
}
