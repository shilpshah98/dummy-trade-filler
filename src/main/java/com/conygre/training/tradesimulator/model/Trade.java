package com.conygre.training.tradesimulator.model;

import java.util.Calendar;

import com.conygre.training.Converter.IdToString;
import com.conygre.training.Converter.StringToId;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Trade {
    public enum Status { 
        CREATED, PROCESSING, PENDING, CANCELLED, REJECTED, FILLED, PARTIALLYFILLED, ERROR
    }

    public enum ActionType{
        BUY,
        SELL
    }

    
    @Id
    @JsonSerialize(converter = IdToString.class)
    @JsonDeserialize(converter = StringToId.class)
    private ObjectId objectId;
    private Calendar dateCreated;
    private Status status = Status.CREATED;
    private String ticker;
    private int quantity;
    private double requestedPrice;
    private ActionType actionType;
    private double totalValue;

    
   
    public Trade(Calendar dateCreated, String ticker, int quantity, double requestedPrice, Status status, ActionType actionType, double totalValue) {
        this.dateCreated = dateCreated;
        this.ticker = ticker;
        this.quantity = quantity;
        this.requestedPrice = requestedPrice;
        this.status = status;
        this.actionType = actionType;
        this.totalValue = totalValue;
    }

    public Trade() {
	}


	public ObjectId getObjectId() {
        return objectId;
    }

    public void setObjectId(ObjectId objectId) {
        this.objectId = objectId;
    }

    public Calendar getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Calendar dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getRequestedPrice() {
        return requestedPrice;
    }

    public void setRequestedPrice(double requestedPrice) {
        this.requestedPrice = requestedPrice;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }


    public double getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(double totalValue) {
        this.totalValue = totalValue;
    }
    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

}
